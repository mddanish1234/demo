//
//  AppDelegate.swift
//  Demo
//
//  Created by SL-MB-3 on 10/10/23.
//

import UIKit
import FirebaseCore
import CoreData
import FirebaseStorage

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let group = DispatchGroup()
    var urls = [URL]()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        
        fetchData()
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func fetchData() {
        let storage = Storage.storage()
        let storageRef = storage.reference(forURL: "gs://fir-74b8a.appspot.com")
        
        group.enter()
        storageRef.child("/department_manager.json").getData(maxSize: 1 * 2048 * 2048) { (data, error) in
            if let error = error {
                print("Error \(error)")
            } else {
                do {
                    guard let dict = try JSONSerialization.jsonObject(with: data!, options: []) as? [[String: Any]] else { return }
                    self.saveEmployeeData(dict: dict, type: "dm")
                } catch {
                    print(error.localizedDescription)
                }
            }
            self.group.leave()
        }

        group.enter()
        storageRef.child("/departments.json").getData(maxSize: 1 * 2048 * 2048) { (data, error) in
            if let error = error {
                print("Error \(error)")
            } else {
                do {
                    guard let dict = try JSONSerialization.jsonObject(with: data!, options: []) as? [[String: Any]] else { return }
                    self.saveEmployeeData(dict: dict, type: "d")
                } catch {
                    print(error.localizedDescription)
                }
            }
            self.group.leave()
        }
        
        group.enter()
        storageRef.child("/emp_departments.json").getData(maxSize: 2 * 2048 * 2048) { (data, error) in
            if let error = error {
                print("Error \(error)")
            } else {
                do {
                    guard let dict = try JSONSerialization.jsonObject(with: data!, options: []) as? [[String: Any]] else { return }
                    self.saveEmployeeData(dict: dict, type: "de")
                } catch {
                    print(error.localizedDescription)
                }
            }
            self.group.leave()
        }
        
        group.enter()
        storageRef.child("/employees.json").getData(maxSize: 2 * 2048 * 2048) { (data, error) in
            if let error = error {
                print("Error \(error)")
            } else {
                do {
                    guard let dict = try JSONSerialization.jsonObject(with: data!, options: []) as? [[String: Any]] else { return }
                    self.saveEmployeeData(dict: dict, type: "e")
                } catch {
                    print(error.localizedDescription)
                }
            }
            self.group.leave()
        }
        
        group.enter()
        storageRef.child("/salaries.json").getData(maxSize: 1 * 2048 * 2048) { (data, error) in
            if let error = error {
                print("Error \(error)")
            } else {
                do {
                    guard let dict = try JSONSerialization.jsonObject(with: data!, options: []) as? [[String: Any]] else { return }
                    self.saveEmployeeData(dict: dict, type: "s")
                } catch {
                    print(error.localizedDescription)
                }
            }
            self.group.leave()
        }
        
        group.enter()
        storageRef.child("/titles.json").getData(maxSize: 1 * 2048 * 2048) { (data, error) in
            if let error = error {
                print("Error \(error)")
            } else {
                do {
                    guard let dict = try JSONSerialization.jsonObject(with: data!, options: []) as? [[String: Any]] else { return }
                    self.saveEmployeeData(dict: dict, type: "t")
                } catch {
                    print(error.localizedDescription)
                }
            }
            self.group.leave()
        }

        group.notify(queue: .main) {
            //print("all requests done")
        }
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {

        let container = NSPersistentContainer(name: "EmployeeDataModel")
        let defaultDirectoryURL = NSPersistentContainer.defaultDirectoryURL()
        
        let deptManagerURL = defaultDirectoryURL.appendingPathComponent("Department_manager.sqlite")
        let deptManagerDescription = NSPersistentStoreDescription(url: deptManagerURL)
        deptManagerDescription.configuration = "Department_manager"

        let deptEmployeeURL = defaultDirectoryURL.appendingPathComponent("emp_departments.sqlite")
        let deptEmployeeDescription = NSPersistentStoreDescription(url: deptEmployeeURL)
        deptEmployeeDescription.configuration = "emp_departments"
        
        let departmentsURL = defaultDirectoryURL.appendingPathComponent("Departments.sqlite")
        let departmentsDescription = NSPersistentStoreDescription(url: departmentsURL)
        departmentsDescription.configuration = "Departments"
        
        let employeesURL = defaultDirectoryURL.appendingPathComponent("Employees.sqlite")
        let employeesDescription = NSPersistentStoreDescription(url: employeesURL)
        employeesDescription.configuration = "Employees"
        
        let salariesURL = defaultDirectoryURL.appendingPathComponent("Salaries.sqlite")
        let salariesDescription = NSPersistentStoreDescription(url: salariesURL)
        salariesDescription.configuration = "Salaries"
        
        let titlesURL = defaultDirectoryURL.appendingPathComponent("Titles.sqlite")
        let titlesDescription = NSPersistentStoreDescription(url: titlesURL)
        titlesDescription.configuration = "Titles"

        container.persistentStoreDescriptions = [deptManagerDescription, deptEmployeeDescription, departmentsDescription, employeesDescription, salariesDescription, titlesDescription]
        container.loadPersistentStores(completionHandler: { (_, error) in
            guard let error = error as NSError? else { return }
            fatalError("###\(#function): Failed to load persistent stores:\(error)")
        })
        
        container.viewContext.automaticallyMergesChangesFromParent = true

        return container
    }()
    
    func saveEmployeeData(dict: [[String: Any]], type: String) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }

        let managedContext = appDelegate.persistentContainer.viewContext
        managedContext.perform {
            for i in 0..<dict.count {
                switch type {
                case "dm":
                    let dm = Department_manager(context: managedContext)
                    
                    dm.dept_no = dict[i]["dept_no"] as? String ?? ""
                    dm.emp_no = dict[i]["emp_no"] as? Int64 ?? Int64(i)
                    dm.from_date = dict[i]["from_date"] as? String ?? ""
                    dm.to_date = dict[i]["to_date"] as? String ?? ""

                    
                case "d":
                    let dept = Departments(context: managedContext)
                    
                    dept.dept_no = dict[i]["dept_no"] as? String ?? ""
                    dept.dept_name = dict[i]["dept_name"] as? String ?? ""
                    
                case "de":
                    let deptEmp = Emp_departments(context: managedContext)
                    
                    deptEmp.dept_no = dict[i]["dept_no"] as? String ?? ""
                    deptEmp.emp_no = dict[i]["emp_no"] as? Int64 ?? Int64(i)
                    deptEmp.from_date = dict[i]["from_date"] as? String ?? ""
                    deptEmp.to_date = dict[i]["to_date"] as? String ?? ""
                    
                case "e":
                    let emp = Employees(context: managedContext)
                    
                    emp.emp_no = dict[i]["emp_no"] as? Int64 ?? Int64(i)
                    emp.birth_date = dict[i]["birth_date"] as? String ?? ""
                    emp.first_name = dict[i]["first_name"] as? String ?? ""
                    emp.last_name = dict[i]["last_name"] as? String ?? ""
                    emp.gender = dict[i]["gender"] as? String ?? ""
                    emp.hire_date = dict[i]["hire_date"] as? String ?? ""
                    
                case "s":
                    let salaries = Salaries(context: managedContext)
                    
                    salaries.emp_no = dict[i]["emp_no"] as? Int64 ?? Int64(i)
                    salaries.salary = dict[i]["salary"] as? Int64 ?? Int64(i)
                    salaries.from_date = dict[i]["from_date"] as? String ?? ""
                    salaries.to_date = dict[i]["to_date"] as? String ?? ""
                    
                case "t":
                    let titles = Titles(context: managedContext)
                    
                    titles.emp_no = dict[i]["emp_no"] as? Int64 ?? Int64(i)
                    titles.title = dict[i]["title"] as? String ?? ""
                    titles.from_date = dict[i]["from_date"] as? String ?? ""
                    titles.to_date = dict[i]["to_date"] as? String ?? ""
                    
                default: break
                }
            }

            do {
                try managedContext.save()
            } catch {
                print("Failed to save test data: \(error)")
            }
        }
    }
}
