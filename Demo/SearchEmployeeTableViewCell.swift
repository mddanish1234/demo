//
//  SearchEmployeeTableViewCell.swift
//  Demo
//
//  Created by Danish on 14/10/23.
//

import UIKit

class SearchEmployeeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var empNameParentView: UIView!
    @IBOutlet weak var empName: UILabel!
    @IBOutlet weak var gender: UILabel!
    @IBOutlet weak var hireDate: UILabel!
    @IBOutlet weak var salary: UILabel!
    @IBOutlet weak var spentYears: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.empNameParentView.layer.cornerRadius = 5.0
        self.empNameParentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        self.empNameParentView.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.empNameParentView.layer.shadowColor = UIColor.lightGray.cgColor
        self.empNameParentView.layer.shadowOpacity = 0.5
        self.empNameParentView.layer.shadowRadius = 3.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    
    func configureCell(_ data: Employees) {
        guard let firstName = data.first_name else { return }
        guard let lastName = data.last_name else { return }
        guard let sex = data.gender else { return }
        guard let hDate = data.hire_date else { return }
        
        empName.text = firstName + " " + lastName
        
        gender.text = sex.lowercased() == "f" ? "Female" : "Male"
        hireDate.text = hDate
        totalSpentYears(hireDate: hDate)
    }
    
    func totalSpentYears(hireDate: String) {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd"
        guard let hDate = formatter.date(from: hireDate) else { return }
        
        spentYears.text = "Spent years: " + "\(yearsBetweenDate(hDate: hDate))"
    }
    
    func yearsBetweenDate(hDate: Date) -> Int {
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year], from: hDate, to: date)

        return components.year ?? 0
    }
}
