//
//  ViewController.swift
//  Demo
//
//  Created by Danish on 10/10/23.
//

import UIKit
import MBProgressHUD
import MaterialComponents.MaterialTextControls_OutlinedTextFields
import CoreData

class ViewController: UIViewController {
    
    @IBOutlet weak var searchTextField: MDCOutlinedTextField!
    @IBOutlet weak var employeeTableView: UITableView!
    @IBOutlet weak var numberOfEmployees: UILabel!
    
    let date = Date()
    let formatter = DateFormatter()
    var datePicker = UIDatePicker()
    var toolBar = UIToolbar()
    
    //MARK: - number of employees who joined after a particular date
    var empCount = 0
    
    private lazy var persistentContainer: NSPersistentContainer = {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        return appDelegate!.persistentContainer
    }()

    private lazy var fetchedResultsController: NSFetchedResultsController<Employees> = {
        let fetchRequest: NSFetchRequest<Employees> = Employees.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "emp_no", ascending: true)]

        let controller = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                    managedObjectContext: persistentContainer.viewContext,
                                                    sectionNameKeyPath: nil, cacheName: nil)
        controller.delegate = self
        do {
            try controller.performFetch()
        } catch {
            fatalError("###\(#function): Failed to performFetch: \(error)")
        }
        return controller
    }()
    
    var fetchData = [Employees]()

    override func viewDidLoad() {
        super.viewDidLoad()
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = .indeterminate
        setupView()
        fetchData = fetchedResultsController.fetchedObjects ?? []
    }

    func setupView() {
        navigationItem.title = "Employees"
        
        self.searchTextField.label.text = "Enter date"
        self.searchTextField.label.textColor = .darkGray
        self.searchTextField.placeholder = "yyyy-MM-dd"
        self.searchTextField.setTextColor(.darkGray, for: .normal)
        
        self.searchTextField.setOutlineColor(.darkGray, for: .editing)
        self.searchTextField.setOutlineColor(.lightGray, for: .normal)
        
        self.searchTextField.delegate = self
        
        self.employeeTableView.delegate = self
        self.employeeTableView.dataSource = self
    }
    
    func showDatePicker() {
        datePicker.datePickerMode = .date
        datePicker.date = Date()
        datePicker.addTarget(self, action: #selector(self.dateChanged(_:)), for: .valueChanged)
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(datePicker)
        
        datePicker.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        datePicker.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        datePicker.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        datePicker.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        formatter.dateFormat = "yyyy-MM-dd"
        
        //self.setMinimumDate(Date())
        //self.searchTextField.text = formatter.string(from: datePicker.date)
        //self.searchTextField.resignFirstResponder()
        
        datePicker.backgroundColor = .white
                    
        toolBar.barStyle = .black
        toolBar.translatesAutoresizingMaskIntoConstraints = false
        toolBar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.onDoneButtonClick))]
        toolBar.sizeToFit()
        self.view.addSubview(toolBar)
        
        toolBar.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        toolBar.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        toolBar.bottomAnchor.constraint(equalTo: datePicker.topAnchor, constant: 0).isActive = true
        toolBar.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    @objc func dateChanged(_ sender: UIDatePicker?) {
        toolBar.removeFromSuperview()
        datePicker.removeFromSuperview()
        if let selectedDate = sender?.date {
            formatter.dateFormat = "yyyy-MM-dd"
            
            self.searchTextField.text = formatter.string(from: selectedDate)
            self.searchTextField.resignFirstResponder()
            
            getSelectedEmployees(selectedDate: selectedDate)
        }
    }

    @objc func onDoneButtonClick() {
        toolBar.removeFromSuperview()
        datePicker.removeFromSuperview()
    }
    
    func setMinimumDate(_ date: Date) {
        self.datePicker.minimumDate = date
    }
    
    func getSelectedEmployees(selectedDate: Date) {
        formatter.dateFormat = "yyyy-MM-dd"
        guard let fetchedResults = fetchedResultsController.fetchedObjects else { return }
        fetchData.removeAll()
        fetchData = fetchedResults.filter({ formatter.date(from: $0.hire_date ?? "")! > selectedDate })

        self.numberOfEmployees.text = "Number of employees: " + "\(self.fetchData.count)"
        employeeTableView.reloadData()
    }
}

// MARK: - NSFetchedResultsControllerDelegate
extension ViewController: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.fetchData = self.fetchedResultsController.fetchedObjects ?? []
        self.numberOfEmployees.text = "Number of employees: " + "\(self.fetchData.count)"
        MBProgressHUD.hide(for: self.view, animated: true)
        self.employeeTableView.reloadData()
    }
}

//MARK: - Handle table view delegates
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchData.count > 400 ? 400 : fetchData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchEmployeeTableViewCell") as? SearchEmployeeTableViewCell else { return UITableViewCell() }
        
        cell.layer.cornerRadius = 7.0
        cell.parentView.layer.cornerRadius = 7.0
        cell.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowRadius = 3
        
        cell.selectionStyle = .none
        
        cell.configureCell(fetchData[indexPath.row])
        
        guard let salaries = fetchData[indexPath.row].salries else { return cell }
        if salaries.count > indexPath.row {
            cell.salary.text = "$" + "\(salaries[0].salary)"
            return cell
        }
        cell.salary.text = "$" + "\(0)"
        
        return cell
    }
}

//MARK: - Handle text field delegate
extension ViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        if self.view.subviews.contains(datePicker) || self.view.subviews.contains(toolBar) {
            toolBar.removeFromSuperview()
            datePicker.removeFromSuperview()
        } else {
            showDatePicker()
        }
    }
}

extension Employees {
    var salries: [Salaries]? { // The accessor of the feedbackList property.
        return value(forKey: "salaries") as? [Salaries]
    }
}
